# K8S + DOCTL + S3CMD

This image mirrors the `alpine/k8s` [Kubernetes toolbox for EKS]([https://hub.docker.com/r/alpine/k8s/), but also installs [Digital Ocean's](https://github.com/digitalocean/doctl) `doctl` CLI tool for working with Digital Ocean's systems.  It also contains `s3cmd`, a Python CLI tool for interacting with Digital Ocean's S3 Spaces.

## Build arguments

- `K8S_VERSION`: Version of `alpine/k8s` image to build from (default: 1.18.2)
- `DOCTL_VERSION`: Version of `doctl` CLI to install;  If not set will search Github for latest tag and use that
