#### 1.19.2 (2020-07-26)

##### Bug Fixes

* **CI:**  Incorrect namespace ([99d40d33](git+ssh://git@gitlab.com/ollycross/k8s-doctl/commit/99d40d33cb85152873ce2f8a2fa5e43ef12f4520))

##### Other Changes

* //gitlab.com/ollycross/k8s-doctl ([5763caeb](git+ssh://git@gitlab.com/ollycross/k8s-doctl/commit/5763caeb0248045371367a21d963958601aa09f7))


#### 1.19.1 (2020-07-26)

##### Bug Fixes

* **CI:**  Incorrect naming ([4d26f532](git+ssh://git@gitlab.com/ollycross/k8s-doctl/commit/4d26f5322ec4939fcdf229736ff8438fcc4941cd))

##### Other Changes

* //gitlab.com/ollycross/k8s-doctl ([73acee40](git+ssh://git@gitlab.com/ollycross/k8s-doctl/commit/73acee401a8f522fdec019e16ed55fac9853da75))


### 1.19.0 (2020-07-26)

##### New Features

* **docker:**  CI push to hub ([44af7c24](git+ssh://git@gitlab.com/ollycross/k8s-doctl/commit/44af7c2492e3917fac30567257154cdf133424e9))
* **bin:**  add s3cmd ([9ca446e9](git+ssh://git@gitlab.com/ollycross/k8s-doctl/commit/9ca446e9e56a708be727ed003d4b1d5d54ae6c17))


