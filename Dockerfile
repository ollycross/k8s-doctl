ARG DOCTL_VERSION=""
ARG K8S_VERSION="1.18.2"

FROM alpine/k8s:$K8S_VERSION
RUN apk add --no-cache curl python3

RUN pip3 install s3cmd

# Install `doctl`
RUN if [[ -z "$DOCTL_VERSION" ]]; \
    then export version=`curl --silent https://api.github.com/repos/digitalocean/doctl/releases/latest | grep '"tag_name":' | sed "s/.*v\([0-9.]\+\).*/\\1/"` ;\
    else export version=$DOCTL_VERSION ; \
    fi && \
    curl --silent -L https://github.com/digitalocean/doctl/releases/download/v${version}/doctl-${version}-linux-amd64.tar.gz  | tar xz && \
    mv ./doctl /usr/bin/doctl

RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2

WORKDIR /app
